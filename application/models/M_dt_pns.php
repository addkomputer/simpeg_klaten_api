<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_dt_pns extends CI_Model {

  public function get_by_id($id)
  {
    return $this->db
      ->select('a.*,b.jabatan,c.eselon,d.golongan,d.pangkat,e.status_pegawai,f.instansi')
      ->join('ms_jabatan b', 'a.id_jabatan = b.id_jabatan','left')
      ->join('ms_eselon c', 'c.id_eselon = right(a.id_jabatan,2)','left')
      ->join('ms_golongan d', 'a.id_golongan = d.id_golongan','left')
      ->join('ms_status_pegawai e', 'a.id_status_pegawai = e.id_status_pegawai','left')
      ->join('ms_instansi f', 'a.id_instansi = f.id_instansi','left')
      ->where('id_pegawai',$id)
      ->get('dt_pegawai a')->row();
  }

  public function update($id,$data)
  {
    $this->db->where('id_pegawai',$id)->update('dt_pegawai a',$data);
  }

}
