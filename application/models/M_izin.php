<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_izin extends CI_Model {

  public function insert($data)
  {
    //cari tipe
    $tipe = $this->db->where('id_tipe',$data['id_tipe'])->get('tb_log_tipe')->row_array();
    //increment tanggal
    $tanggal = $data['tgl_mulai'];
    $mulai = date_create($data['tgl_mulai']);
    $akhir = date_create($data['tgl_akhir']);
    $diff  = date_diff($mulai,$akhir)->format('%d');
    $pegawai = $this->db->where('id_pegawai',$data['id_pegawai'])->get('dt_pegawai')->row();
    $data['id_instansi'] = $pegawai->id_instansi;
    unset($data['tanggal'],$data['tgl_mulai'],$data['tgl_akhir']);
    for ($i=0; $i <= $diff; $i++) { 
      $data['tanggal'] = date('Y-m-d', strtotime($tanggal. '+'.$i.' days'));
      $suffix = substr($data['tanggal'],0,4);
      $data['bulan'] = substr($data['tanggal'],5,2);
      $table = $tipe['table'].'_'.$suffix;
      //insert table per kateogori
      $this->create_table($table);
      //cek table 
      $cek_tipe = $this->db
        ->where('tanggal',$data['tanggal'])
        ->where('id_pegawai',$data['id_pegawai'])
        ->get($table)->row();
      if($cek_tipe){
        //update
        $this->db
          ->where('tanggal',$data['tanggal'])
          ->where('id_pegawai',$data['id_pegawai'])
          ->update($table,$data);
      }else{
        //insert
        $this->db->insert($table,$data);
      }
      //table kehadiran
      $this->create_table_kehadiran($suffix);
      $cek_kehadiran = $this->db
        ->where('tanggal',$data['tanggal'])
        ->where('id_pegawai',$data['id_pegawai'])
        ->get('tb_log_kehadiran_'.$suffix)->row();
      if($cek_kehadiran){
        //update
        $this->db
          ->where('tanggal',$data['tanggal'])
          ->where('id_pegawai',$data['id_pegawai'])
          ->update('tb_log_kehadiran_'.$suffix,$data);
      }else{
        //insert
        $this->db->insert('tb_log_kehadiran_'.$suffix,$data);
      }
    }
  }

  public function create_table($table)
  {
    $this->db->query(
      "CREATE TABLE IF NOT EXISTS `$table` (
        `id` bigint(20) NOT NULL AUTO_INCREMENT,
        `bulan` varchar(2) NOT NULL,
        `id_pegawai` varchar(50) NOT NULL DEFAULT '0',
        `nomor_induk` varchar(50) DEFAULT '0',
        `pin` varchar(50) DEFAULT '0',
        `id_instansi` int(11) DEFAULT '0',
        `tanggal` date NOT NULL,
        `id_tipe` char(2) DEFAULT NULL,
        `file_bukti` varchar(100) DEFAULT NULL,
        `keterangan` text,
        `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `created_by` varchar(50) NOT NULL DEFAULT 'System',
        `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
        `updated_by` varchar(50) DEFAULT NULL,
        `is_active` tinyint(1) NOT NULL DEFAULT '1',
        `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
        PRIMARY KEY (`id`)
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1;"
    );
  }

  public function create_table_kehadiran($suffix)
  {
    $this->db->query(
      "CREATE TABLE IF NOT EXISTS `tb_log_kehadiran_$suffix` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `bulan` varchar(2) NOT NULL,
        `id_pegawai` varchar(50) DEFAULT '0',
        `nomor_induk` varchar(50) DEFAULT '0',
        `pin` varchar(50) DEFAULT '0',
        `id_instansi` int(11) DEFAULT '0',
        `id_tipe` int(1) DEFAULT '-1',
        `id_shift` int(11) DEFAULT '7',
        `is_tukar` tinyint(1) DEFAULT '7',
        `kode` varchar(50) DEFAULT 'P3',
        `tanggal` date DEFAULT NULL,
        `jam_datang` time DEFAULT NULL,
        `jam_batas_datang` time DEFAULT NULL,
        `is_datang` tinyint(1) NOT NULL DEFAULT '0',
        `jam_datang_pegawai` time DEFAULT NULL,
        `terlambat_datang` int(11) DEFAULT '0',
        `ket_datang` text,
        `is_pulang` tinyint(1) NOT NULL DEFAULT '0',
        `jam_pulang` time DEFAULT NULL,
        `jam_batas_pulang` time DEFAULT NULL,
        `jam_pulang_pegawai` time DEFAULT NULL,
        `mendahului_pulang` int(11) DEFAULT '0',
        `ket_pulang` text,
        `file_bukti` varchar(100) DEFAULT NULL,
        `keterangan` text,
        PRIMARY KEY (`id`)
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;"
    );
  }

}