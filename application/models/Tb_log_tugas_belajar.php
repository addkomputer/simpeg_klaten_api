<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tb_log_tugas_belajar extends CI_Model {

  public function get_all($id_pegawai = null,$tahun)
  {
    $suffix = $tahun;
    if ($this->db->table_exists("tb_log_tugas_belajar_$suffix"))
    {
      return $this->db->query(
        "SELECT a.*, b.tipe AS tipe_nm 
        FROM tb_log_tugas_belajar_$suffix a
        LEFT JOIN tb_log_tipe b ON a.id_tipe=b.id_tipe
        WHERE 
          id_pegawai='$id_pegawai'"
      )->result();
    }else{
      return null;
    }
  }

}