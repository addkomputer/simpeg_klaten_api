<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dt_pegawai extends CI_Model {

  public function get_all($id = null)
  {
    if ($id === null) {
      return $this->db->query(
        "SELECT id_pegawai as id, gelar_depan, nama, gelar_belakang FROM dt_pegawai"
      )->result();
    }else{
      return $this->db->query(
        "SELECT id_pegawai as id, gelar_depan, nama, gelar_belakang FROM dt_pegawai WHERE id_pegawai='".$id."'"
      )->result();
    }
  }

  public function get_by_id($id_pegawai)
  {
    return $this->db->query(
      "SELECT * FROM dt_pegawai WHERE id_pegawai='$id_pegawai'"
    )->row();
  }

  public function update($id_pegawai,$data)
  {
    $this->db
      ->where('id_pegawai',$id_pegawai)
      ->update('dt_pegawai',$data);
  }

}