<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_ringkasan extends CI_Model {

  public function get_all($id_pegawai)
  {
    $suffix = date('Y');
    $bulan = date('m');

    if ($this->db->table_exists('tb_log_kehadiran_'.$suffix)){
      $jumlah_tahun = $this->db->query(
        "SELECT *
        FROM tb_log_kehadiran_$suffix
        WHERE 
          id_pegawai='".$id_pegawai."'"
      )->num_rows();
    }else{
      $jumlah_tahun = 0;
    }

    if ($this->db->table_exists('tb_log_kehadiran_'.$suffix)){
      $jumlah_bulan = $this->db->query(
        "SELECT *
        FROM tb_log_kehadiran_$suffix
        WHERE 
          id_pegawai='".$id_pegawai."'"
      )->num_rows();
    }else{
      $jumlah_bulan = 0;
    }

    if ($this->db->table_exists('tb_log_kehadiran_'.$suffix)){
      $date = new DateTime();
      $awal = $date->modify('this week')->format('Y-m-d');
      $akhir = $date->modify('this week +6 days')->format('Y-m-d');
      $jumlah_minggu = $this->db->query(
        "SELECT *
        FROM tb_log_kehadiran_$suffix
        WHERE 
          (tanggal >= '$awal' AND tanggal <= '$akhir') AND
          id_pegawai='".$id_pegawai."'"
      )->num_rows();
    }else{
      $jumlah_bulan = 0;
    }

    if ($this->db->table_exists('tb_log_jaldin_'.$suffix)){
      $jumlah_jaldin = $this->db->query(
        "SELECT *
        FROM tb_log_jaldin_$suffix
        WHERE 
          id_pegawai='".$id_pegawai."'"
      )->num_rows();
    }else{
      $jumlah_jaldin = 0;
    }

    if ($this->db->table_exists('tb_log_tugas_belajar_'.$suffix)){
      $jumlah_tugas_belajar = $this->db->query(
        "SELECT *
        FROM tb_log_tugas_belajar_$suffix
        WHERE 
          id_pegawai='".$id_pegawai."'"
      )->num_rows();
    }else{
      $jumlah_tugas_belajar = 0;
    }

    if ($this->db->table_exists('tb_log_sakit_'.$suffix)){
      $jumlah_sakit = $this->db->query(
        "SELECT *
        FROM tb_log_sakit_$suffix
        WHERE 
          id_pegawai='".$id_pegawai."'"
      )->num_rows();
    }else{
      $jumlah_sakit = 0;
    }

    if ($this->db->table_exists('tb_log_cuti_'.$suffix)){
      $jumlah_cuti = $this->db->query(
        "SELECT *
        FROM tb_log_cuti_$suffix
        WHERE 
          id_pegawai='".$id_pegawai."'"
      )->num_rows();
    }else{
      $jumlah_cuti = 0;
    }

    if ($this->db->table_exists('tb_log_bencana_'.$suffix)){
      $jumlah_bencana = $this->db->query(
        "SELECT *
        FROM tb_log_bencana_$suffix
        WHERE 
          id_pegawai='".$id_pegawai."'"
      )->num_rows();
    }else{
      $jumlah_bencana = 0;
    }

    if ($this->db->table_exists('tb_log_apel_'.$suffix)){
      $jumlah_apel = $this->db->query(
        "SELECT *
        FROM tb_log_apel_$suffix
        WHERE 
          id_pegawai='".$id_pegawai."'"
      )->num_rows();
    }else{
      $jumlah_apel = 0;
    }

    if ($this->db->table_exists('tb_log_ganti_'.$suffix)){
      $jumlah_ganti = $this->db->query(
        "SELECT *
        FROM tb_log_ganti_$suffix
        WHERE 
          id_pegawai='".$id_pegawai."'"
      )->num_rows();
    }else{
      $jumlah_ganti = 0;
    }

    $res = array(
      'jumlah_tahun' => $jumlah_tahun,
      'jumlah_bulan' => $jumlah_bulan,
      'jumlah_minggu' => $jumlah_minggu,
      'jumlah_jaldin' => $jumlah_jaldin,
      'jumlah_tugas_belajar' => $jumlah_tugas_belajar,
      'jumlah_sakit' => $jumlah_sakit,
      'jumlah_cuti' => $jumlah_cuti,
      'jumlah_sisa_cuti' => 16 - $jumlah_cuti,
      'jumlah_bencana' => $jumlah_bencana,
      'jumlah_ganti' => $jumlah_ganti,
      'jumlah_apel' => $jumlah_apel
    );

    return $res;
  }

}