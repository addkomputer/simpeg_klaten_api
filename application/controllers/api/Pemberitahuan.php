<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Pemberitahuan extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('dt_pemberitahuan');
    }

    public function index_get()
    {
        $id_pegawai = $this->get('id_pegawai');
        $id_article = $this->get('id_article');

        if ($id_article == null) {
            $data = $this->dt_pemberitahuan->get_all($id_pegawai);
    
            if (!empty($data))
            {
                $this->response($data, REST_Controller::HTTP_OK);
            }
            else
            {
                $this->set_response([
                    'status' => FALSE,
                    'message' => 'Data could not be found'
                ], REST_Controller::HTTP_NOT_FOUND);
            }
        }else{
            $data = $this->dt_pemberitahuan->get_detail($id_article);
    
            if (!empty($data))
            {
                $this->response($data, REST_Controller::HTTP_OK);
            }
            else
            {
                $this->set_response([
                    'status' => FALSE,
                    'message' => 'Data could not be found'
                ], REST_Controller::HTTP_NOT_FOUND);
            }
        }
        
    }

}
