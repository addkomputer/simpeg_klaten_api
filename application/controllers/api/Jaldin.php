<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Jaldin extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('tb_log_jaldin');
    }

    public function index_get()
    {
        $id_pegawai = $this->get('id_pegawai');
        $tahun = $this->get('tahun');
        
        $data = $this->tb_log_jaldin->get_all($id_pegawai,$tahun);

        if (!empty($data))
        {
            $this->response($data, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'Data could not be found'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

}
