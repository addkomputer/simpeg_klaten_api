<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Upload extends REST_Controller {

    function __construct()
    {
        parent::__construct();
    }

    public function index_post()
    {
        $config['upload_path']          = FCPATH."../simpeg_klaten/berkas/bukti";
		$config['allowed_types']        = 'gif|jpg|jpeg|png|pdf';
		$config['overwrite']			= true;
		$config['max_size']             = 100000000;
		$config['remove_spaces'] 		= TRUE;
		$config['encrypt_name'] 		= TRUE;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('file_upload')){
			$error = array('error' => $this->upload->display_errors());
            $res = array(
                "status" => FALSE,
                "message" => $error
            );
		}else{
			$succ = $this->upload->data();
            $res = array(
                "status" => TRUE,
                "message" => $succ
            );
		}
	
        $this->set_response($res, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

}
