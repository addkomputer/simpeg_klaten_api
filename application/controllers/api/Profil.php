<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Profil extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('dt_pegawai');
    }

    public function index_get()
    {
        $id_pegawai = $this->get('id_pegawai');

        if ($id_pegawai === NULL)
        {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST);
        }

        if ($id_pegawai == '')
        {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST);
        }
        
        $data = $this->dt_pegawai->get_by_id($id_pegawai);

        if (!empty($data))
        {
            $this->response([
                'status' => TRUE,
                'data' => $data
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'Data could not be found'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function index_post()
    {
        $data = $_POST;
        unset($data['key']);
        $id = $data['id_pegawai'];
        $this->dt_pegawai->update($id,$data);
        $this->response([
            'status' => TRUE,
            'message' => 'Data Updated'
        ], REST_Controller::HTTP_OK);
    }

}
