<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Instansi extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('ms_instansi');
    }

    public function index_get()
    {
        $id = $this->get('id');

        if ($id === NULL)
        {
            $data = $this->ms_instansi->get_all();

            if ($data)
            {
                $this->response([
                    'status' => TRUE,
                    'data' => $data
                ], REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No data were found'
                ], REST_Controller::HTTP_NOT_FOUND);
            }
        }

        if ($id == '')
        {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST);
        }
        
        $data = $this->ms_instansi->get_all($id);

        if (!empty($data))
        {
            $this->response([
                'status' => TRUE,
                'data' => $data
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'Data could not be found'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

}
