<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Ringkasan extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_ringkasan');
    }

    public function index_get()
    {
        $id_pegawai = $this->get('id_pegawai');

        if ($id_pegawai === NULL || $id_pegawai == '')
        {
            $this->response([
                'status' => FALSE,
                'message' => 'Invalid Parameter(s)'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }

        $data = $this->m_ringkasan->get_all($id_pegawai);

        if (!empty($data))
        {
            $this->response($data, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'Data could not be found'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

}
