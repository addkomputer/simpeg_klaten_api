<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Change_pass extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_dt_pns');
    }

    public function index_post($id_pegawai=null)
    {   
        $data = array(
            'passwd' => md5($this->post('passwd')),
            'passwd_ulang' => $this->post('passwd_ulang')
        );
        //
        unset($data['passwd_ulang']);
        //
        $update = $this->m_dt_pns->update($id_pegawai,$data);
        $res = array(
            "status" => TRUE,
            "message" => "Password Changed"
        );
        $this->set_response($res, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

}
