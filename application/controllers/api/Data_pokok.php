<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Data_pokok extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_dt_pns');
    }

    public function index_get()
    {
        $id_pegawai = $this->get('id_pegawai');
        
        $data = $this->m_dt_pns->get_by_id($id_pegawai);
        // validate photo
        if ($data->photo != 'no-photo.jpg') {
            $data->path = "http://addkomputer.com/simpeg_klaten/berkas/photo/";
        }else{
            $data->path = "http://addkomputer.com/simpeg_klaten/img/";
        }

        if (!empty($data))
        {
            $this->response($data, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'Data could not be found'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function index_post($id_pegawai=null)
    {   
        $data = array(
            'alamat' => $this->post('alamat'),
            'kode_pos_rumah' => $this->post('kode_pos_rumah'),
            'no_hp' => $this->post('no_hp'),
            'npwp' => $this->post('npwp')
        );

        $update = $this->m_dt_pns->update($id_pegawai,$data);
        $res = array(
            "status" => TRUE,
            "message" => "Data Updated"
        );
        $this->set_response($res, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

}
