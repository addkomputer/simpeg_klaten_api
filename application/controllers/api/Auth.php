<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Auth extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_auth');
    }

    public function index_post()
    {
        $d_auth = array(
            'nomor_induk' => $this->post('nomor_induk'),
            'passwd' => md5($this->post('passwd'))
        );

        $auth = $this->m_auth->login($d_auth);
		if($auth->num_rows() >= 1){
            $pegawai = $auth->row_array();
            $res = array(
                'id_pegawai' => $pegawai['id_pegawai'],
                'id_status_pegawai' => $pegawai['id_status_pegawai'],
                'gelar_depan' => $pegawai['gelar_depan'],
                'nama' => $pegawai['nama'],
                'gelar_belakang' => $pegawai['gelar_belakang']
            );
        }else{
            $res = array(
                'status' => false,
                'message' => 'failed authentication'
            );
        }

        $this->set_response($res, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

}
