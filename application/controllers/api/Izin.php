<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Izin extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_izin');
    }

    public function index_post()
    {
        $data = array(
            'id_instansi' => $this->post('id_instansi'),
            'id_pegawai' => $this->post('id_pegawai'),
            'tgl_mulai' => $this->post('tgl_mulai'),
            'tgl_akhir' => $this->post('tgl_akhir'),
            'id_tipe' => $this->post('id_tipe'),
            'keterangan' => $this->post('keterangan'),
            'file_bukti' => $this->post('file_bukti')
        );

        $izin = $this->m_izin->insert($data);
        $res = array(
            "status" => TRUE,
            "message" => "Data Inserted"
        );
        $this->set_response($res, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

}
