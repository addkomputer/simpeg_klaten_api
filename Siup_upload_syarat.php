<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class siup_upload_syarat extends CI_Controller{

	function __construct() {
		parent::__construct();
		//		
	}
	
	function index() {
		$siup_id 		= @$_POST['siup_id'];
		$kategori_siup 	= @$_POST['kategori_siup'];
		//
		$file_source 	= @$_FILES['uploaded_file']['name'];
		$param_syarat 	= @$_POST['param_syarat'];
		//
		if (empty($siup_id)) { 
			header('Content-Type: application/json');
			//
			$response = array(
				'success' => 0,
				'message' => 'Data tidak boleh kosong'
			);
			echo json_encode($response);
		} else {
			// $file_type = get_file_type($file_source);
			$file_type = 'pdf';
			$file_source_nm = 'dkp.jateng.'.$param_syarat.'.'.md5(date('Y-m-d H:i:s')).'-'.$file_source;
			$dir_kategori_siup = ($kategori_siup == 'O' ? 'perorangan' : 'perusahaan');
			$path = 'assets/files/siup/'.$dir_kategori_siup.'/'.$file_source_nm;
			//
			// process
			$data['siup_id'] = $siup_id;
			$data['param_syarat'] = $param_syarat;
			$data['file_source'] = $file_source_nm;
			$query = $this->db->insert('data_siup_syarat', $data);
		

			if ($query){
	/*			file_put_contents($path, $file_source);*/
				move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $path);
				//
				header('Content-Type: application/json');
				//
				$response = array(
					'success' => 'success',
					'message' => 'Data berhasil diupload.'
				);
				echo json_encode($response);
			} else{ 
				header('Content-Type: application/json');
				//
				$response = array(
					'success' => 'failed',
					'message' => 'Gagal upload file.'
				);
				echo json_encode($response);
			}
		}	
	}
	
}