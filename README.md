# SIMPEG KLATEN API

Key : 7d063bfb561bda7078d649dfeafe9aee

### Auth
- Login <br>
http://{server_ip}/simpeg_klaten_api/api/auth<br>
Method : POST <br>
Parameter : 
  - key
  - nomor_induk
  - passwd

### Jabatan
- Get All Jabatan <br>
http://{server_ip}/simpeg_klaten_api/api/jabatan<br>
Method : GET <br>
Parameter :
  - key
- Get Specific Jabatan <br>
http://{server_ip}/simpeg_klaten_api/api/jabatan<br>
Method : GET <br>
Parameter : 
  - key
  - id

### Instansi
- Get All Instansi <br>
http://{server_ip}/simpeg_klaten_api/api/instansi<br>
Method : GET <br>
Parameter :
  - key
- Get Specific Instansi <br>
http://{server_ip}/simpeg_klaten_api/api/instansi<br>
Method : GET <br>
Parameter : 
  - key
  - id

### Profil
- Get Profil <br>
http://{server_ip}/simpeg_klaten_api/api/profil<br>
Method : GET <br>
Parameter :
  - key
  - id_pegawai
- Update Profil<br>
Method : POST
Parameter : :
  - key
  - id_pegawai
  - other field (mengikuti field hasil dari get profil)


### Ringkasan
- Get All Ringkasan <br>
http://{server_ip}/simpeg_klaten_api/api/ringkasan<br>
Method : GET <br>
Parameter :
  - key
  - id_pegawai

### Tipe
- Get All Tipe <br>
http://{server_ip}/simpeg_klaten_api/api/tipe<br>
Method : GET <br>
Parameter :
  - key
- Get Specific Tipe <br>
http://{server_ip}/simpeg_klaten_api/api/tipe<br>
Method : GET <br>
Parameter : 
  - key
  - id

### Upload
- Post Upload <br>
http://{server_ip}/simpeg_klaten_api/api/upload<br>
Method : POST <br>
Parameter :
  - key
  - file_upload

### Izin
- Post Izin <br>
http://{server_ip}/simpeg_klaten_api/api/izin<br>
Method : POST <br>
Parameter :
  - key
  - id_pegawai
  - tgl_mulai
  - tgl_akhir
  - id_tipe
  - keterangan
  - file_bukti

### Jaldin
- Get Jaldin <br>
http://{server_ip}/simpeg_klaten_api/api/jaldin<br>
Method : GET <br>
Parameter :
  - key
  - id_pegawai
  - tahun
  - bulan

### Tugas Belajar
- Get Tugas Belajar <br>
http://{server_ip}/simpeg_klaten_api/api/tugas_belajar<br>
Method : GET <br>
Parameter :
  - key
  - id_pegawai
  - tahun
  - bulan

### Sakit
- Get Sakit <br>
http://{server_ip}/simpeg_klaten_api/api/sakit<br>
Method : GET <br>
Parameter :
  - key
  - id_pegawai
  - tahun
  - bulan

### Cuti
- Get Cuti <br>
http://{server_ip}/simpeg_klaten_api/api/cuti<br>
Method : GET <br>
Parameter :
  - key
  - id_pegawai
  - tahun
  - bulan

### Bencana
- Get Bencana <br>
http://{server_ip}/simpeg_klaten_api/api/bencana<br>
Method : GET <br>
Parameter :
  - key
  - id_pegawai
  - tahun
  - bulan

### Ganti
- Get Ganti <br>
http://{server_ip}/simpeg_klaten_api/api/ganti<br>
Method : GET <br>
Parameter :
  - key
  - id_pegawai
  - tahun
  - bulan

### Apel
- Get Apel <br>
http://{server_ip}/simpeg_klaten_api/api/apel<br>
Method : GET <br>
Parameter :
  - key
  - id_pegawai
  - tahun
  - bulan


### Data Pokok
- Get Data Pokok <br>
http://{server_ip}/simpeg_klaten_api/api/data_pokok<br>
Method : GET <br>
Parameter :
  - key
  - id_pegawai


- Edit Data Pokok <br>
http://{server_ip}/simpeg_klaten_api/api/data_pokok/{id_pegawai}<br>
Method : POST <br>
Parameter :
  - key
  - alamat
  - kode_pos_rumah
  - no_hp
  - npwp


- Ganti Password <br>
http://{server_ip}/simpeg_klaten_api/api/change_pass/{id_pegawai}<br>
Method : POST <br>
Parameter :
  - key
  - passwd
  - passwd_ulang

### Pemberitahuan
- Get Pemberitahuan <br>
http://{server_ip}/simpeg_klaten_api/api/pemberitahuan<br>
Method : GET <br>
Parameter :
  - key
  - id_pegawai